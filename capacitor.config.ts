import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.nousclic.friendly',
  appName: 'friendlyapp',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
