import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Injectable({
  providedIn: 'root'
})
export class PushService {
  usedId: string;
  ext: string;

  constructor(
    private oneSignal: OneSignal,
    public http: HttpClient,
    private router: Router
  ) { }

  configuracionInicial(uid: string) {
    /* this.oneSignal */
    this.oneSignal.setExternalUserId(uid);
    this.oneSignal.startInit('3c86c400----------------------------');
    this.oneSignal.enableSound(true);
    this.oneSignal.enableVibrate(true);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      //Cuando se Recibe
      this.ext = noti.payload.additionalData['ruta'] + '';
    });

    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      //Abre la notificacion
      this.router.navigate([this.ext]);
    });
    //obtener id de onesignal
    this.oneSignal.getIds().then((info) => {
      this.usedId = info.userId;
    });
    this.oneSignal.endInit();
  }

  //Enviar notificaciones individuales

  public sendByUid(title: string, subtitle: any, detalle: string, ruta: string, uiduser: string) {
    //envio de info
    const datos = {
      app_id: '',
      included_segments: ['Active User', 'Inactive User'],
      headings: { en: title },
      subtitle: { en: subtitle },
      data: {ruta: ruta, fecha: '100-210-250'},
      contents: {en:detalle},
      channel_for_external_user_ids: 'push',
      include_external_user_ids: [uiduser],
    };
    this.post(datos);
  }

  deleteuser(){
    this.oneSignal.removeExternalUserId();
  }

  //Enviar notificaciones Global

  public sendGlobal(title: string, subtitle: any, detalle: string, ruta: string){
    //Enviar notificacion
    const datos = {
      app_id: '',
      included_segments: ['Active User', 'Inactive User'],
      headings: { en: title },
      subtitle: { en: subtitle },
      data: {ruta: ruta, fecha: '100-210-250'},
      contents: {en:detalle},
    };
    this.post(datos);
  }

  post(datos: any){
    const headers = {
      Authorization: 'Basic '
    };
    const url = 'https://onesignal.com/api/v1/notifications';
    return new Promise((resolve)=>{
      this.http.post(url, datos, {headers}).subscribe((data)=>{
        resolve(data);
      });
    });
  }
  //Enviar notificaciones globales
}
