export interface User {
    uid?: string;
    name?: string;
    email?: string;
    phoneNumber?: string;
    role?: string;
    address?: string;
    dateOfBirth?: Date;
    documentType?: string;
    identificactionDocument?: string;
    image?: string;
    gender?: string;
    isActive?: boolean;
    createdAt: Date;
    updatedAt: Date;
}
