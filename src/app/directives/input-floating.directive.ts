import {
  Directive,
  ElementRef,
  HostListener,
  ViewChild,
  Renderer2,
  NgModule,
  Input,
} from '@angular/core';
import { AnimationController, DomController } from '@ionic/angular';

@Directive({
  selector: '[appInputFloating]',
})
export class InputFloatingDirective {
  input: any;
  label: any;

  constructor(
    public element: ElementRef,
    private animationCtrl: AnimationController,
    private domCtrl: DomController,
    private renderer: Renderer2
  ) {}

  ngAfterViewInit() {
    let content = this.element.nativeElement;
    this.label = content.getElementsByClassName('label')[0];
    this.input = content.getElementsByTagName('ion-input')[0];
    this.domCtrl.read(() => {
      if (this.input.value !== '') {
        this.renderer.setStyle(this.label, 'color', '#4D4C4C');
        this.animationUp().play();
      }
    });
  }

  /* Add an event in the input */
  @HostListener('ionFocus', ['$event']) onFloatingInputUp($event) {
    const animation = this.animationUp();
    if (this.input.value !== '') {
      return;
    }
    this.renderer.setStyle(this.label, 'color', '#4D4C4C');
    animation.play();
  }

  @HostListener('ionBlur', ['$event']) onFloatingInputDown($event) {
    const animation = this.animationDown();
    if (this.input.value !== '') {
      return;
    }
    this.renderer.setStyle(this.label, 'color', '#b6b6b6');
    animation.play();
  }

  animationUp() {
    const animation = this.animationCtrl.create();
    animation
      .addElement(this.label)
      .fill('forwards')
      .duration(150)
      .fromTo('transform', 'translateY(0px)', 'translateY(-25px)');
    return animation;
  }

  animationDown() {
    const animation = this.animationCtrl.create();
    animation
      .addElement(this.label)
      .fill('forwards')
      .duration(220)
      .fromTo('transform', 'translateY(-25px)', 'translateY(0px)');
    return animation;
  }
}
