import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputFloatingDirective } from './input-floating.directive';

@NgModule({
  declarations: [InputFloatingDirective],
  imports: [CommonModule],
  exports: [InputFloatingDirective],
})
export class SharedDirectivesModule {}
