import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { rejects } from 'assert';
import { Observable } from 'rxjs';
import app from "firebase/app";
import "firebase/authio";
import { userInfo } from 'os';
import { PushService } from '../services/notification/push.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private PushService: PushService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> {
    return new Promise((resolve, rejects) => {
      app.auth().onAuthStateChanged((user: app.User) => {
        if (user) {
          if (user.emailVerified) {
            this.PushService.configuracionInicial(user.uid);
            resolve(true);
          } else {
            this.router.navigate(['/login']);
          }
        } else {
          this.router.navigate(['/login']);
          resolve(false);
        }
      });
    });
  }
}