import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from '../auth/login/login-routing.module';

import { LoginPage } from './login.page';
import { InputModule } from 'src/app/components/input-form/input/input.module';
import { InputFloatingDirective } from 'src/app/directives/input-floating.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InputModule,
    InputFloatingDirective,
    LoginPageRoutingModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
